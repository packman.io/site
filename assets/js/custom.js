
//
$(document).ready(function() {
			$('#fullpage').fullpage({
				'verticalCentered': false,
				'scrollingSpeed': 600,
				'autoScrolling': false,
				'fitToSection': false,
				'css3': true,
				'navigation': true,
				'navigationPosition': 'right',
				'fitToSectionDelay': 10000,
			});
		});

// wow
$(function()
{
    new WOW().init();
    $(".rotate").textrotator();
})
