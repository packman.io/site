# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "packman.io"
  spec.version       = "0.0.21"
  spec.authors       = ["Jesse Portnoy"]
  spec.email         = ["jesse@packman.io"]

  spec.summary       = "A simple portfolio style index page and blog"
  spec.homepage      = "https://gitlab.com/packman.io/site"
  spec.license       = "AGPLv3"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", ">= 3.8.5"

  spec.add_development_dependency "bundler", "~> 2.0.1"
  spec.add_development_dependency "rake", "~> 12.0"
end
