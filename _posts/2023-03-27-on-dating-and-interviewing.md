---
layout: post
title: Would you reply to a message from someone on a dating site with "Why do you want to marry me?"?
date: 2023-03-27 -0000
background: '/img/posts/blackboard.jpg'
---

I've noticed that many job applications require you to submit a short essay in order to apply and that some include the curious question: *"Why do you want to work for us?"*. Let me tell you something about this question - it is akin to replying to a message from someone on a dating site with: *"Why do you want to marry me?"*. 

Would you ask someone that question on a dating site? I doubt it. If you did, you'd probably never hear from that person again. Looking for a job is very similar to dating. Both parties need to be interested and reasonable people do not assume that everyone in the world wants them. 

The fact someone messaged you, be it on a dating site or via a job application, does not mean they want to marry you or work for you, it just means they can see a scenario in which they would be interested in doing so. Now, it's up to both parties to explore that option. 

It's okay to ask why a person thinks they're a good fit during an interview but to answer that question with even a semblance of accuracy, one would need to know some basic facts about the role, the people one will be working with and the daily tasks one would partake in. Some job descriptions do include information about the role, trouble is, they all say it's an amazing company and that the role is exceptionally wonderful and the crew is divine so, unless we're to believe all roles in all companies are absolutely brilliant, it's not something one can go on. 

Right, now that we got this out of the way, I'd like to talk about what I feel is the ideal interview process for technical people.

- A short conversation to confirm the details of the role: technical stack, location (on-site/hybrid/remote), company size, salary range, etc
- A home assignment that reflects what one will be doing on the job. 

Here's why this is important:
A job interview should assess how successful a candidate will be in performing their actual role. In my case, that means writing code, maintaining code written by others and performing various related tasks such as setting up machines, testing and deployment of said code, monitoring performance and failures and corresponding with others on all of the above. 

All developers have access to the internet, where they can read documentation, technical posts of different natures (questions, howtos, etc) and machines where they can compile/run their code through the interpreter and conduct some tests. No developer does his/her work on a bloody blackboard and decent developers do not (usually) implement their own sorting algorithms from scratch. 

Therefore, there's no point in showing them a code snippet on a piece of paper and asking them: "What will this output?" or inquiring as to the efficiency of a bubble sort. These sort of questions do not reflect what they'll actually be doing and their success (or failure) in answering them therefore predicts nothing. 

**At this point, you may think to yourself: but they can copy the answers off the internet! Well, yes, if they are good, efficient developers, there's no doubt they WILL in fact use the internet to research and write their code. That's the job. You do not WANT them to implement EVERYTHING from scratch. That would take far longer and is likely to produce lesser results than those previously tested and honed.** But, here comes the important bit:

- After they've submitted the task and assuming you were pleased with its quality, you invite them for an interview. At this point, you can ask them questions about the assignment. For instance: 
    - Why did you choose to do it this way?
    - What other ways can you think of? 
    - Did you find this task interesting and why?
    - Insert other relevant questions here

This manner of testing is good because it:
- Puts the candidate at ease (they only have to explain their own work after all, which should be easy)
- Tests not only the ability to write code but also to communicate your work to others, which, I reckon everyone will agree is important
- Unlike the "why do you want to work here?" question, succeeding in such a process does show their commitment to excelling at the interview and hopefully, at the job as well

A favourite of mine for such a task? Present someone with real code from your own code base and ask them to explain what it does and how to improve upon it.

- Lastly, I think it's also good to have the candidate meet with key team members to ascertain basic chemistry. People who like each other can achieve far more (in less time) than people that do not.


On the occasions I was asked to interview candidates, that's exactly the approach I took and the results were most satisfactory.
 
If you agree with my assertions and have a role to offer, I am looking so, do get in touch.

Good luck to everyone on the hunt.
