---
layout: post
title: On changes, differences and risk management
date: 2023-04-01 15:42:19 UTC
background: //cdn-images-1.medium.com/max/1024/0*WZgTPWijiinMUlx9
---
Everyone who knows me, even slightly, will tell you that I do not like change. It’s a narrative that I myself promote but in reality, it’s a simplification of a more complex condition. I typically avoid simplifications as I find it offends both parties but even I, the abstraction and metaphor detester, have to make some compromises — mostly because I cannot force everyone I meet to read the below&nbsp;post.

Like I said, I often tell (and later remind) people that I hate changes and to be honest, while I always had an inkling that this is not the most accurate statement, the first time I ever thought about it in depth was a few days&nbsp;ago.

There I was, articulating to myself (yet again) exactly why I feel like I do about this time in my life and one of my arguments as to why it’s so difficult to handle was that I don’t do well with changes; but then I thought: if you are so averse to change, why do you so enjoy modifying existing code, that, seemingly, at least, works okay in order to improve&nbsp;it?

And I do enjoy that! This is a repeating pattern for me and one of the things that I feel make me good at my job and, for certain tasks, better suited than others of similar knowledge and intellect — I very much delight in fixing and optimising code (especially one that I had nothing to do with until that point) whereas many others, who would NOT assert that they dislike change (and may very well proclaim their affection towards it), tend to avoid it as much as possible and, when forced to do so, demonstrate trepidation and in large quantities at&nbsp;that!

I reflected on that for a bit and quickly realised that what I actually loathe and — more to the point — fear, is risks, not&nbsp;changes.

In my line of work, risks can be managed quite dependently; they can usually be mitigated and often avoided altogether. You can have as many branches in your code repo as you wish (and you can also take a more archaic route and simply copy the whole directory over to a different path — a route I still opt for quite often, especially late at night), you can back your resource files (data, media, what have you) and configuration up, you can automate testing these backup and recovery procedures and manually test them on a QA/UAT (your term here) ENV before making any changes in Production.

Sometimes, due to a combination of bad engineering, laziness and lack of foresight and unreasonable commitments, you do not have backups for the data nor a testing ENV that is a true representation of the Production one. I faced these situations on occasion during my long career and indeed — I did not enjoy working in such conditions, made as few changes as humanly possible and escaped as soon as I&nbsp;could.

If you made it this far, you may have nodded at some of these sentences but either way, you’re probably wondering: “Yeah, okay, this is all true but — what’s your point, Jesse? What I am to take away from this&nbsp;post?”

Good question. Maybe you think I’m trying to educate you as to the importance of backups and contingency plans. Or, perhaps, you reckon I’m suggesting that, if you’re like me, a career in programming would be a good refuge from other aspects of life. Well, while I do think that’s all true, this is not what has compelled me to write&nbsp;this…

As usual with me, I’m writing this because I am bothered and what I am bothered by is why my feelings towards risks are deemed so special (and not in a positive manner) when every sociologist, psychologist and psychiatrist would undoubtedly agree that everyone is afraid of&nbsp;risks.

Before I continue, here’s a little quote from Christopher Boone, the protagonist of “The curious incident of the dog at night-time”, which is basically a story about a 15 year old me (I have Asperger’s too):

_“All the other children at my school are stupid. Except I’m not meant to call them stupid, even though that is what they are. I’m meant to say that they have learning difficulties or that they have special needs. But this is stupid because everyone has learning difficulties because learning to speak French or understanding relativity is difficult and also everyone has special needs, like Father, who has to carry a little packet artificial sweetening tablets around with him to put in his coffee to stop him from getting fat, or Mrs. Peters who wears a beige-coloured hearing aid, or Siobhan, who has glasses so thick that they give you a headache if you borrow them, and none of these people are Special Needs, even if they have special needs (pp.&nbsp;43–44).”_

So yes, everyone has special needs and everyone is afraid of risks and all experts would agree with that. Why then, is my way of thinking so hard for many people to&nbsp;accept?

I do not have a definitive answer. If I did, it would no longer bother me as much; I am typically most disturbed by things I do not fully understand and that too, I think is true to most people. Some just do a better job at suppressing these questions altogether.

And still, since we’ve made it to this point, here’s why I think people find my behaviours and opinions where it comes to changes/new things hard to&nbsp;handle:

It all comes down to different scoring methods. When faced with a decision, of any kind, we ALL perform some calculations to arrive at a course of action. We typically do it very quickly and without analysing the process but I will now attempt to give you a glance into mine with a very simple, yet indicative example.

Here’s a common, ever so frequent question: “Where shall we go to&nbsp;lunch?”

Simple, right? Good. My answer will vary depending on whom I am with and I will now give all common variations.

When with someone I love (or at least very much like) and trust: _“Whatever you fancy. It is all about the company for&nbsp;me.”_

That’s what I will invariably say and I will mean it, too (alas, it takes time to teach people not to question it). I do not have a complex relationship with food. I eat it to fuel my body and when I’m with someone I love and trust, I know that if I can’t figure out what to eat (I don’t care about food but there are things I will not eat — like all these bloody herbs and garnishes that everyone seems so fascinated with consuming) they will help me find something and that, since I will enjoy my time with them, I will probably not pay much attention to other factors in my immediate environment that may very well cause me stress were I to be there with strangers.

When with strangers/random acquaintances:

- If I am in familiar surroundings, I will ALWAYS, name a place I know well and is nearby and, assuming my suggestion is accepted, I will ALWAYS order the exact same&nbsp;thing
- When in unfamiliar territory (say if we’ve travelled for a work conference), I will NEVER make a suggestion or express a preference since I don’t have any data to go on and no interest in researching venues or negotiating with the group and I will only ever go if there’s absolutely no reasonable way to avoid&nbsp;it.

So, that’s my risk scoring algorithm. Ask a “normal” person (for lack of a better word — I debated between normal and average and could write a whole post on such definitions alone) the same question and you’ll likely notice two things immediately:

- Most people will opt to try a NEW PLACE whenever circumstances and time allow it or at least, a place they have not been to for some&nbsp;time
- If you ask them to assess the risk of doing so — they will laugh.&nbsp;Hard

Here are some of the risks I see when evaluating this simple scenario (and I do emphasise, I deliberately chose a very easy, seemingly risk free situation to make my&nbsp;point):

- The walk over to the venue will be long and I wouldn’t be able to fit into the conversation at&nbsp;all
- The walk over will be long and I will be forced to engage in conversation I do not enjoy, all the while desperately trying to keep a neutral face as to not offend anyone (and it’s hard to do because, ironically, while I cannot read facial expressions, I am told I have a very expressive face)
- Both of the above also apply to the meal itself, of&nbsp;course
- I will not be able to find anything to eat (and I will not trust anyone enough to admit that and ask for help as developing trust for me is a very precise process that takes&nbsp;time)
- The place will be too crowded and&nbsp;loud
- I will go for a fag or the loo and wouldn’t be able to find my way back to the table (sounds ridiculous, right? it happens to me very often and stresses me out even though I know it to be&nbsp;silly)
- Someone will make a remark on a seemingly immaterial topic that will make me either very angry or hurt and no one will understand why (that is, if I even choose to express these sentiments)
- Same thing only the other way round: I will be making the&nbsp;remark..

And these are just the obvious ones, I could think of&nbsp;others.

That was a short demonstration of what I mean by risk scoring method and why I often catalogued as odd, difficult or, what sometimes hurts the most — negative or not&nbsp;fun.

To recap and, at the risk of committing yet another oversimplification sin: for the most part, people associate **new** with **interesting and fun** rather than **risky** and well, as you can gather from the above — I am different that&nbsp;way.

If you made it to the end, I sincerely hope you found this at least somewhat interesting and not too depressing and that, if you have someone like me in your life (or you ARE someone like me) this will encourage you to better understand them (or yourself, as the case may be). I’d like to end with another Christopher Boone quote I&nbsp;like:

_“And Siobhan says people go on holidays to see new things and relax, but it wouldn’t make me relaxed and you can see new things by looking at earth under a microscope or drawing the shape of the solid made when 3 circular rods of equal thickness intersect at right angles. And I think that there are so many things just in one house that it would take years to think about all of them properly. And, also, a thing is interesting because of thinking about it and not because of being&nbsp;new.”_

 ![](https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=5bb3d3d255cb)