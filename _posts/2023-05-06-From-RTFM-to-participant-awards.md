---
layout: post
author: Jesse Portnoy
title: From RTFM to participant awards
date: 2023-05-06 18:11:35 UTC
background: //cdn-images-1.medium.com/max/1024/1*OXzQLMIcNy1IturaT-EKYw.png
---

This is a story of the decline of our standards. I expect the opinions I express here may be controversial but honestly? I don’t mind it. I’m all for discussion and while I’d rather only receive supportive comments (anyone that says they want to be disagreed with is lying), I’m open to opposing opinions as well, so long as we keep it polite and on&nbsp;point.

To those who don’t know the acronym, RTFM stands for Read The Fucking Manual. This used to have been a perfectly reasonable response to give when someone asked a stupid or super trivial question that is covered in the documentation.

I am not a huge fan of expletives and use them very rarely but since RTM is not as effective in driving the point home, I suppose, had I coined this acronym, I’d probably use “bloody” (Americans often think “bloody” is just the UK version of “fucking” but it isn’t so). But I&nbsp;digress…

At a certain point in time, it somehow became impolite to use RTFM and now, it appears to be perceived as plain rude. This is not because of the “fucking” bit at all. If anything, it seems that cursing is becoming more and more acceptable, not less.   
So then, what is the reason for this change in norm and, why is it so important?

Well, let’s consider the notion this acronym conveys so succinctly; You’ve asked a stupid/trivial question. This implies&nbsp;that:

> You have not read the manual **which you are expected to do** , and now, because you’re too lazy to read what I have read to be in a position to answer you (or even written myself for others to read, as the case may be), you’re needlessly wasting my valuable&nbsp;time

or:

> You have read the manual and still managed not to know the answer to this basic question so… come on, don’t make me say&nbsp;it.

At this point, you may be thinking: “_Jesse, that’s unfair! Maybe the questioner DID read the manual and it’s simply badly written?_”

To this I say: indeed, manuals are written by people and people make mistakes. Thinking that’s something will be obvious to others because it is to YOU is a very common and human mistake to make. HOWEVER, when that’s the case, I’d expect the question to be prefixed&nbsp;with:

> “I read the manual and in particular THIS section and couldn’t find the answer. I then proceeded to Google my question and looked at multiple results but still couldn’t nail it down. Could you please help&nbsp;me?”

If you prefixed your question in that manner and still got an RTFM reply then, you’re right, it’s unfair but, that’s hardly ever the&nbsp;case.

Another case I’m fine with and will never respond to with RTFM is when someone who is a friend or at least, a very close peer, prefixes the question&nbsp;with:

> “I know it’s probably in the manual but, you’re quicker to ask and I’m very stressed at the&nbsp;moment…”

I’ll absolutely accept that. As humans, our behaviour is subjective and friends are more important than&nbsp;manuals.

These exceptions asides, basically, replying with RTFM berates the questioning party for failing to adhere to fundamental expectations: read manuals to learn what to do and, more implicitly — do not disturb others needlessly.

The reason RTFM is now considered rude is because we no longer expect that of people, which is extremely sad and yes, **important**. The rest of this post is an account of my opinion as to why and how we got to a point where we no longer have basic standards and expectations where it comes to learning.

### It all starts with the bloody participant awards

I remember the first time I heard about participant awards. It was in an article written about a book discussing education and its results. Unfortunately, it was a rather long time ago (over 10 years) and I cannot find it, which is a shame. I have not actually read the book (or I’d certainly remember its title and author name), only the article about it. I tried Googling key phrases from it in an attempt to locate it but couldn’t. If anyone knows the book I’m describing, please&nbsp;comment.

At any rate, for those of you that don’t know, it has become common practice to give children an award, not for being the best at something but merely, for SHOWING UP. Yes, that’s right. A competitive event is held, one child performs better than all the others, others perform well, others still perform poorly and EVERYONE gets an&nbsp;award.

When I was growing up, we did not get awards for showing up and learning that this is now done drove me absolutely bonkers. At that time, what worried me (and the author of this book I cannot find) was that this sends the wrong message to children and will produce adults that cannot handle the real world. What I failed to predict was that, instead, the world will be changed by these very children.

Let’s take a step back here and talk about the generation of my parents (just to frame this — I am 41). My grandfather was my sole male role model growing up. I idolised him, I still do and he absolutely deserves it. As a child. he taught me how to do pretty much everything but computer programming, which I taught myself (I don’t believe he ever touched a computer, frankly but I think, had he been born a bit later, he’d have loved it). He was always kind, patient and accepting. I owe him everything. He never hit me or any of my siblings and I can only remember ONE occasion in which he yelled at us and even then, it was only because he was concerned that we’re disturbing my&nbsp;nan.

Why do I bring this up in this context? Because my mum once told me that, instructed by nan, he used to hit her and her siblings. My first reaction was complete shock and I was even inclined to believe that she’s lying (we’ve a very complicated relationship and I don’t trust her) but upon reflection, I suppose it’s probably true. I’m POSITIVE that he didn’t want to do that but yes, it was a different time and hitting children was considered an acceptable and even educational form of punishment. That proved to be bad practice so, we stopped doing it, which is a good&nbsp;thing!

Every generation thinks they are the best and that the preceding and subsequent ones are rubbish. I am aware of this and am not an exception to the rule. Further, the blessed influence of my granddad aside, I had a most rotten childhood so, I will not bring my memories of how my parents handled me as an example to support any of the observations I make in the following few paragraphs (I do lean into my personal experience towards the end of this post though — it’s unavoidable).

Having said that, one’s parents are not the sole influence on one’s young life and, reflecting on the general messages I have absorbed growing up, it was obvious that physical violence towards children (or anyone for that matter) is discouraged. It was equally clear that in order to get praise and do well, one has to work hard. I can’t recall an incident where the underlying message was that everyone is amazing and it’s enough to just SHOW&nbsp;UP.

I don’t mean to imply, by any means, that the educational methods applied to me and my peers were perfect. Far from it. And I have mates that suffered unjustly because of various syndromes I believe they now call learning disabilities (forgive me if it’s not the correct PC terms at the time of writing this, it’s hard to keep&nbsp;track).

So, that was my world growing up. Imperfect and often unfair towards those who are substantially different (substantially because everyone is different in one way or the other and feels&nbsp;it).

In some ways, one could say, perhaps rightly, that things are better now; at least in terms of accepting diversity of thought and abilities.   
For example, I have Asperger’s. I went through my entire childhood and much of my adult life without being diagnosed. I was finally diagnosed at the age of 30, by a psychiatrist whom I came to consult about a, then new to me, syndrome called OCD. Would I have been better off growing up now? In that particular sense, perhaps. It’s possible that a young Jesse growing up today would have been diagnosed as autistic, rather than just labelled as weird and, it would have probably been a very good thing. At least in my case. I do know that this diagnosis, late as it was, has helped me: I can now refer people to a Wikipedia page and get a more favourable treatment when I behave “weirdly”.

But I think we’re now taking it to the other extreme. It seems to me that everyone (parents, teachers, the whole world and his sister) are afraid to give any criticism lest they hurt the child’s fragile soul. And this is what the participant award is all about. We’re so fearful that, if we were to say John or Jill did best at a competition, the other children will get upset and will be scarred for life so instead, we give EVERYONE an award. THIS IS&nbsp;WRONG.

It’s unfair to John and Jill because we’re not acknowledging their hard work and talent and it’s unfair to all the other children because we’re not encouraging them to find something they excel at. Everyone cannot be special at everything. If everyone is special then — NO ONE&nbsp;IS.

People of all ages want to be acknowledged for their special abilities and efforts and they want to be guided as to how to find and capitalise on&nbsp;these.

If you’re told that everyone is equally amazing and special, you’re essentially being told that there’s no reason to work hard. Which brings us back to the RTFM point. People don’t read the manual because, as children, they learned, from these very incidents and messages, that they don’t have to work hard to be successful or appreciated and that they, like everyone else, is&nbsp;amazing.

Let’s focus on this one word: amazing. It seems to be used so.. liberally these days. So much so that, one day, I’ve actually looked its definition up just to establish that I haven’t gotten it wrong all these years! Here it&nbsp;is:

> Causing great surprise or wonder; astonishing.  
> Informal: very impressive; excellent.

Right. So, as I suspected, I did not get it wrong. When I use the word amazing, which I am incredibly discerning about, I mean exactly that: very impressive, excellent. I use it sparingly, because, let’s face it, most things are NOT very impressive or excellent. We have many other words to describe things that are not THAT; for example: ordinary, regular, normal, usual (and dozens if not hundreds of other words, in EVERY language).

To this day, and even after obtaining all the above insights, I am still extremely disappointed, borderline crestfallen even, when, after having “amazing” used in reference to something I have worked hard on and required skill, the same word is used to describe a most minimal&nbsp;effort.

Giving undeserved accolades and awards is hurting everyone, including the very people you’re trying to make feel good by doing so. Let me give you an&nbsp;example:

I like music. Not as much as I like programming but I like it. I started coding when I was 9. Not only did I receive no encouragement from my parents, I was actually discouraged by my mother (who thought and probably still does, that computers are an utter waste of time) and my efforts were further hindered by the fact that my sperm donor (I know him, I just don’t call him father) occupied our only computer through large portions of the day playing video&nbsp;games.

If you’re reading this and thinking that it’s very sad, I agree with you but here’s my actual point: when I was 10, I took some guitar lessons. This, I WAS encouraged to do because my mother liked music (presumably, I don’t recall her ever actually listening or commenting on music but I also don’t recall her saying it’s a waste of time) and my sperm donor was allegedly a rather gifted guitar player (I say allegedly because, as you’ll soon come to realise, I am in no position to judge such abilities). My mum paid for these lessons and I don’t recall having to beg or even vigorously ask for it (programming I learned by myself from reading programmes that came with QBASIC and, when I was 14, after having programmed a bit with QBASIC, I had to BEG her quite a bit until she agreed to pay for a book about&nbsp;C).

Anyway, I was no good. I don’t recall anyone actively telling me that I am rubbish at it (maybe they did and it just didn’t matter enough to register) but I definitely knew I am not very good and I felt no inner compulsion to work hard it either so, soon enough, I stopped the&nbsp;lessons.

While it would have been nice to be able to play well, imagine a world where, although I was no good, I’d have been told that I am amazing at it. Well, for one thing, whether you’re 10 years old or 41, we all like praise. I’d have probably kept playing (badly, while wasting my mum’s most limited funds) and maybe, I’d even develop a ridiculous dream (ridiculous in light of my lack of talent, I’m not saying NO ONE should cultivate that dream, there are, after all successful, excellent musicians that made an impact on many people, myself included) of becoming a rock star. Why is that bad?&nbsp;Because:

> a. I am not talented at playing the guitar and could never, no matter how hard I worked, become amazing enough to succeed professionally

> b. Because of my desire to excel, I’d be immensely frustrated by&nbsp;a.

> c. I would have given less attention to something that I AM good at, love and make a decent living from, to wit: programming

Now, is it unfortunate that my mother did not encourage my natural tendencies? Very. Would I have become a happier adult if she had? ABSOLUTELY. But, would taking the opposite approach and encouraging me to stick with something I am clearly NOT good at resulted in a better outcome? ABSOLUTELY NOT.

And the same is true to how we engage with adults in all capacities. We should never tell someone they’re amazing at something when they’re not just to be “nice” or to avoid insulting them. Misleading someone isn’t nice. It’s counterproductive; to their well-being, yours, the organisation and society at&nbsp;large.

We SHOULD expect people to read manuals and learn on their own. We should not spoon-feed them and, if we’ve given them ample opportunity to perform and they haven’t, we should tell them so. I’m not saying we should be brutal and make them cry, of course. Giving constructive criticism is a very complex art, indeed, one that managers, teachers and parents should invest time in perfecting, but avoiding it because it doesn’t feel “nice” is not a solution.

Here’s to applying yourself,

 ![](https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=f956308ebe97)
