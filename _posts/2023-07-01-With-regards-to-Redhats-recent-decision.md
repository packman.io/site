---
layout: post
author: Jesse Portnoy
title: With regards to Redhat’s recent decision…
date: 2023-07-01 19:03:34 UTC
background: //cdn-images-1.medium.com/max/1024/0*teapQ9C6w2Ohd7xb
---

In case you’ve not been following, the decision in question is to [limit access to the Red Hat Enterprise Linux source&nbsp;code](https://www.phoronix.com/news/Red-Hat-CentOS-Stream-Sources).

This caused a mini storm; with this article, I aim&nbsp;to:

- Succinctly explain this move and its implications (hint — not as dramatic as some people make them&nbsp;seem)
- Offer my opinion as to why RH decided on this&nbsp;approach

### **The actual&nbsp;move**

Below is a quote from [RH’s official press&nbsp;release](https://www.redhat.com/en/blog/furthering-evolution-centos-stream):

> _We are continuing our investment in and increasing our commitment to CentOS Stream._ **_CentOS Stream will now be the sole repository for public RHEL-related source code releases._** _For Red Hat customers and partners, source code will remain available via the Red Hat Customer&nbsp;Portal._

Okay, two questions may come to the minds of those reading&nbsp;this:

- What exactly is this **_CentOS&nbsp;Stream?_**
- What’s the source code in question?

Let’s start with the first question; from [https://www.centos.org/centos-stream](https://www.centos.org/centos-stream):

> Continuously delivered distro that tracks just ahead of Red Hat Enterprise Linux (RHEL) development, positioned as a midstream between Fedora Linux and&nbsp;RHEL.

This may confuse some people because CentOS was, to use a kind term, repurposed a while back and those who have not followed that change will find this definition doesn’t match their understanding of what it is. Basically, to rephrase the above: **CentOS Stream is RHEL’s upstream.**

Right. Now what sources are we talking about? After all, RHEL is a Linux distribution (and one of many, at that); it does not own the kernel (which is provided under the terms of the GNU General Public _License_ version 2) so, when the word “source” is used in this context, what does it refer&nbsp;to?

I imagine anyone who is interested in this topic has at least some vague idea as to what a Linux distribution means but I’ll provide my own little blurb here. **Before I do that, however, I should note that there are different kinds of distributions; Redhat is a binary software distribution, whereas Gentoo (to name the leader of its kind) is based on the idea that you compile sources on your own machine using its** [Portage package management system](https://wiki.gentoo.org/wiki/Portage).

> A “binary” Linux distribution takes the kernel sources, as well as those of many many other projects/components (GCC, BASH, libpng, Golang, Rust, PHP, X-server. GNOME, KDE, Xbill, etc) and packages them in a way that’s easy to deploy and&nbsp;upgrade.

In the case of RH, the packaging format is RPM (Redhat Package Manager). RPM, while a Redhat invention, is also FOSS and is used and contributed to by many other Linux distributions, as well as independent developers. Another example for a commonly used FOSS project conceived at Redhat is logrotate (there are many&nbsp;others).

Okay, so Redhat (as well as many other distros) builds and packages a multitude of FOSS components (some of which it also developed and maintains) so that we won’t have to do it ourselves. As you can imagine, this takes a lot of work (there are many packages!) and, as they’re doing it, they encounter bugs, to which they apply patches (before compiling). They then contribute these patches upstream (when appropriate — sometimes, the patch is only needed in order to package the source for a Redhat distribution).

So, “source” in this context mainly pertains to the RPM specs used to generate the packages and the patches applied during the build process. RPM Source packages are also referred to as&nbsp;SRPMs.

**Now that we understand what CentOS Stream and “sources” mean in this context, who will be affected by this decision?**

As emphasised previously, there are many binary Linux distributions. One, very broad, way of segmenting these would be based on the packaging format (and accompanying tool-chains) they use; with the two most common ones being: deb and&nbsp;RPM.

_Side note: deb is my format of choice, conceived by Debian, my favourite distro. One example of another distro that uses this format is Ubuntu. I could (and perhaps should) write a whole article about the relationship between Debian and Ubuntu and another that compares between these two packaging formats but neither pertain to this RHEL decision so, I’ll leave it at that for now. I have started writing a series of articles about software packaging; if you’re interested in this topic, you can find the first instalment_ [_here_](https://medium.com/@jesse_62134/docker-is-not-a-packaging-tool-e494d9570e01)_._

As noted above, RPM is Redhat’s packaging format of choice and there are many different distros that use it as well. Some of these distros consider being binary compatible with RHEL releases their main selling point. I use the term “selling” quite loosely here, as in many cases, no money is exchanging hands.

To better understand this rather intricate ecosystem, let’s consider how Redhat generates (large portions of) its revenue. There are two main&nbsp;streams:

- Professional Services
- Technical Support

To demonstrate the value of these two propositions, let’s consider a person I know intimately: myself(!).

As you already know if you got this far, I am a Debian user. I’ve used it exclusively on all my personal machines and when the choice was mine to make, on all servers under my control as&nbsp;well.

Again, I could write volumes about why I love Debian as much as I do but I (and Debian) am not the topic of this particular article so I’ll be brief and say that the main factors&nbsp;are:

- It’s a community distro that matches my&nbsp;ideology
- Its release cycle and repo segmentation make sense to&nbsp;me
- I find deb (and the accompanying tool-chain) superior to that of RPM (and its tool-chain)
- The package quality is extraordinary; regardless of whether you compare it with commercial distros or fellow community ones

Having said that, would I choose Debian if I were the CTO of a vast financial consortium with thousands of technical staff? No, I’d actually choose&nbsp;Redhat.

Why? Because while I, as a one man band, can easily maintain a 100 Debian servers of different configurations and purposes (and even more than that by putting in place automated procedures), this imaginary CTO version of me (to be clear: I’ve no aspiration to become that person) cannot. He also cannot personally interview his 1000+ employees to ensure that only like minded people that can are on the payroll. You see, Debian has many hard working, bright volunteers and they produce excellent packages and, if you know how to investigate, solve and report issues, you will also get superb support from the community but, if you don’t, you’re better off paying for a Redhat subscription. In return for your subscription fees, Redhat’s support will squeeze the information out of you/your employees like one squeezes tooth paste out of the tube. They will not send you off with a friendly RTFM (see [this article](https://medium.com/@jesse_62134/from-rtfm-to-participants-awards-f956308ebe97) for my views on how important the RTFM notion is) and you could also report back to the board and say: “It’s being looked at by RH” and be sure that no one will fault you for anything. Google “No one ever got fired for purchasing IBM” for more on the latter&nbsp;point.

Right, so, that’s why, in my opinion, people opt for&nbsp;RHEL.

Now, who is affected by this decision? The RHEL&nbsp;clones.

At this point, you may be thinking: “Okay, Jesse, got it but, if the only reason to pay for RHEL is so you could benefit from professional services and tech support (and arse coverage), why would you opt for one of its binary compatible clones instead?”

Once more, **in my view,** two use&nbsp;cases:

- As a contractor working with a company that uses RHEL, I want to work on the closest thing to it without paying for a subscription
- As the aforementioned CTO, I want my tech chaps to have the closest thing to it on their dev machines without paying for a subscription (on Prod, I’ll pay, to get the benefits we already&nbsp;covered)

It is well worth noting that this move by Redhat is unlikely to eradicate these so-called clones, it will merely make life a bit more difficult for&nbsp;them.

From [https://www.phoronix.com/news/Rocky-Linux-RHEL-Source-Access](https://www.phoronix.com/news/Rocky-Linux-RHEL-Source-Access):

> “These methods are possible because of the power of GPL. No one can prevent redistribution of GPL software. To reiterate, both of these methods enable us to legitimately obtain RHEL binaries and SRPMs without compromising our commitment to open source software or agreeing to TOS or EULA limitations that impede our rights. Our legal advisors have reassured us that we have the right to obtain the source to any binaries we receive, ensuring that we can continue advancing Rocky Linux in line with our original intentions.”

Now we arrive at the other question…

### Why did the Redhat take this&nbsp;step?

Redhat does not care about the community clones. It knows that people using these will not buy a Redhat subscription if they were to disappear. They’ll go with Debian (or Ubuntu or one of the numerous RPM based distros available).

**Who do they care about?** [**Oracle Enterprise Linux**](https://en.wikipedia.org/wiki/Oracle_Linux) **.**

From [https://en.wikipedia.org/wiki/Oracle\_Linux](https://en.wikipedia.org/wiki/Oracle_Linux):

> Oracle Linux (abbreviated OL, formerly known as Oracle Enterprise Linux or OEL) is a Linux distribution packaged and freely distributed by Oracle, available partially under the GNU General Public License since late 2006.[4] It is compiled from Red Hat Enterprise Linux (RHEL) source code, replacing Red Hat branding with Oracle’s.

**Do I support Redhat’s decision? No** , I don’t. When you base your business model on FOSS (see [https://medium.com/@jesse\_62134/dont-forget-to-floss-25f3faa3856 for why I think it’s the best choice)](https://medium.com/@jesse_62134/dont-forget-to-floss-25f3faa3856e), you enjoy many benefits (too many to list in this article) but, you also need to prepare yourself for things like Oracle Enterprise Linux.

Can I understand how Redhat would be irked by Oracle Enterprise Linux? Absolutely but again, it’s part of the&nbsp;deal.

_Psst.._

_Liked this post and have a role I could be a good fit for? I’m open to suggestions. See_ [_https://packman.io/#contact_](https://packman.io/#contact) _for ways to contact&nbsp;me._

_Cheers,_

 ![](https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=270b2cf80706)
