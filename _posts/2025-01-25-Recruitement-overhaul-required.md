--- 
layout: post
title: Recruitment - overhaul required, urgently
date: 2025-01-25 19:13:06 UTC
background: /img/overhaul.jpg
---

The recruitment industry is due for a massive overhaul.

This has been the case for many years, possibly decades; however, recent circumstances made it far more pressing.
Many people, myself included, have been ranting about it but I've yet to see a concrete plan to address the current state of affairs. 

In this proposed series of articles (perhaps the word manifest is warranted), I will attempt to break the problem into its sub-components and propose detailed, possible solutions. I will focus on my industry (to wit: software development - stick with what you know is always a good rule); however, many of my salient points also apply to other domains and I will leave this extrapolation as an exercise to the reader (for now, at any rate).

At times, for the sake of brevity, rather than detail everything that is being done wrong, I will simply outline how it should be done, instead. 
Comments are most welcome.

## Candidate Filtering

The first step when compiling a pool of quality candidates is to provide a concise, accurate job description. This is the cornerstone of the entire process and failing to do so will severely hinder the overall process and have a heavy effect on the overall result. **If you can't communicate what you're looking for, your chances of finding it dramatically decrease**. 
 
### Job Descriptions

Job descriptions must be written by those who, at least at some point in their career, have done the job or, failing a perfect match (perfection is to be chased, hardly ever to be achieved), a comparable job, with contributions from those who are now managing people who perform this exact role or one most similar to it. As with many things (though not all), the quality of the description is likely to improve by requesting inputs from multiple individuals in different roles. 

For example, when looking for a PHP developer to join a team of five, the description should be compiled based on inputs from:
- The direct manager of said team
- Two leading team members (establishing who these two are is a challenge in its own right; one that accounts for many of the problems covered in this essay but we'll get to that)
- One or two representatives from units the team in question works most closely with (QA, Project Management, Account Management, Support, etc)

Why is this so important?
At the risk of stating the obvious, the objective of this elaborate process is to locate candidates with the highest chance of succeeding on the job. For this, one must first describe the day-to-day nature of the role:

- Compensation range (a meaningful one - not 50k-120k) and location (on-site, hybrid [and if so where] or remote).
 
- The technical work; which, in the case of software development, is done chiefly by conversing with machines (if that's not the case for your company, you're doing it wrong!). This type of communication, however, has many subtleties. While all programming languages (and this applies to frameworks and operating systems as well) share many common similarities, and a good programmer can certainly adapt and learn on the go (in fact, if one can't, one should seriously consider a change in direction), there are also many differences and, if one truly loves this particular vocation, one will develop specific opinions, tastes and preferences and these will have a significant effect on one's productivity and satisfaction. As I write this, I am so tempted to illustrate this point with detailed examples; I will delay my gratification for the time being and instead, state the bottom line: **the job description must include a high-level description of the technical stack one will be working with**. The stack will change over time (revisit the point about adaptability) but, in most cases, not drastically.  

- The human interfaces; with the former point in mind, ultimately, we do not write software to entertain machines
  (that's the lovely thing about machines - they haven't such needs); we write software for (and often with) humans,
with the aid of machines. A programmer is, in essence, an interface between the machine and various human players
(end-users and company stakeholders). A role can be most interesting and satisfying from a technical standpoint and
still be a nightmare if the human elements are misaligned. The job description should strive to depict this
collaboration as accurately as possible; to wit: what sort of people will one collaborate with as part of the role, using what means/protocols/systems?

This last statement (put in the form of a question) requires clarification, lest it be misunderstood. By "what sort of people", I do not mean a meaningless list of superlatives (best, talented, kindest, brightest, supporting, open-minded, you get the idea). There's an astounding amount of that tripe in many job descriptions (and resumes). Not only is this meaningless (self-praise without supporting evidence always is), it is, in fact, **counterproductive**, as it makes the description far longer and is typically placed in the beginning, which means that, by the time you get to the important bits (if you haven't given up altogether), you suffer from fatigue.  

- A succinct blurb about the company. With succinct being the operative word (again, not an abundance of self-praise and
  lengthy paragraphs about its love for diversity and support of world peace). Just facts: the vertical/industry, clientele,
headcount, location, product line. And, on the subject of diversity and world peace, here's a universal truth: people who emphasise their support of these things have a problem they are trying to compensate for. Those who truly
support equality (which leads to diversity) do not feel the need to explicitly state it. It's like recounting sexual
endeavours often and in great detail without the slightest prompting. It's a tell. 

To recap, a job description must:

- Be written by the people who do the actual work (to put bluntly - not professional recruiters and certainly NOT HR)
- Be limited to: compensation and location, technical outline and the human characteristics and propensities (not superlatives and meaningless accolades!) of those involved as well as those expected from the successful candidate

**Important note**: the order in which these elements are written also matters. I chose to list compensation and
location first, not because I believe it's necessarily the most crucial consideration but because it's the shortest, while still
being a major factor. To illustrate the importance of accuracy here, lying about this aspect is akin to using a picture
of Brad Pitt in your dating profile (assuming you do **not** look like that, of course, hence the "lying" bit), you're just wasting everyone's time and, as with dating, the candidate will not fall in love with you and forget all about this deception because of your winning personality. 

As the overall problem is complex, I think it best to outline it in easily digestible instalments and this seems like a
good stopping point. 

In the [next instalment](https://packman.io/2025/02/05/Recruitement-overhaul-required-1.html), I will discuss the current approach towards resumes and why it so painfully fails to serve the end goal.
Some salient points for reflection:

- While the means of initial filtering drastically changed (from a manual review by a competent human to automatic
  parsing by software), the format has not changed at all and, worst still, there's no universally adhered standard to
use for the parsing/data extraction process
- With the advent of a multitude of programming languages, each with loads of popular frameworks and constructs, technology has become both far more complex and complicated and the filtering algorithms are not advancing accordingly
- Emphasis on the wrong, least significant aspects during the initial candidate evaluation (hint: self-praise yet
  again, with too little supporting evidence)

As noted in my opening statement, I welcome comments and discourse and have started formulating a plan to address this issue. I firmly believe it's more than feasible and, from a technical standpoint (the human aspects are a different story), not overly complex. If you care, please take part and voice your opinions. 

