---
layout: post
title: Whatever happened to "be descriptive"?
date: 2023-01-29 -0000
background: '/img/posts/describe.jpg'
---

Some thoughts with regards to ridiculous job descriptions I've encountered on LI in recent years (it's an ongoing and worrying trend):

I am **NOT** a ninja (had some Judo classes when I was 9, if that helps you any) nor am I a rock star (I had some guitar lessons when I was 10; alas, I am tone deaf and my motor skills are limited to typing on keyboards and shooting pool).

I am not exceptional in this, either; to the best of my knowledge NONE of my engineering mates/colleagues are ninjas or rock stars (although some do play an instrument or practice a marshal art at some capacity, it's true). I am rather confident something like being a ninja or rock star would have come up after knowing someone for say, longer than a fortnight.                                                                                                                    

Further, I am not sure what use a hi-tech company has for ninjas and rock stars. I mean, sure, it would make for a jolly good show in the company's talent competition and probably nice for marketing campaigns but otherwise, I daresay these particular qualifications are unnecessary/borderline useless when writing software.

These buzzwords aren't cool, they are, at best, silly and redundant and often enough, borderline abusive. Filtering that rubbish out in an attempt to understand whether a job is relevant to a person is wasteful, annoying and counter productive.

One person's opinion, obviously, but I for one, will refrain from applying to any position described in these terms.

If you're curious as to what I am (as opposed to what I'm not - ninja, rock star): 

I am a multidisciplinary programmer (they call it full-stack nowadays but I prefer the former term) with a clear preference towards backend development, a solid UNIX (that includes Linux, of course) system administration experience and basic understanding of hardware (I have never designed HW but I know how to put it together and apart and what common parts do and how to test them for issues).

Good wishes to everyone presently on the hunt.
