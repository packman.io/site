---
layout: post
title: Matrix Reloaded - Reflections
date: 2025-02-04 14:53:37 UTC
description: matrix, absolutely no emotion whatsoever, unrealistic hack scene, CRC32 exploit, trinity, ssh, imap, autistic
background: //nmap.org/movies/matrix/trinity-nmapscreen-hd-crop-1200x728.jpg
---

Amazon Prime currently offers all Matrix films for free.
I watched the trilogy in my late teens and early twenties. What left the biggest impression on me back then wasn't the films themselves (honestly, other than "they have really cool combat scenes" there's nothing impressive to say) but rather an observation that a mate in our group made. He said: there's absolutely no emotion here whatsoever. For those who don't know, I'm autistic and to be honest, at that point in my life, I didn't quite understand what he meant by that. I'm still autistic, of course, but it is known that many of us do improve (a controversial word) in the emotional department over time. It's not that my thought patterns changed but one of the things people with Asperger's tend to excel at is pattern-matching and I now have a far larger sample to analyse and compare with so watching it 22 years later, I do see it. It IS a super weird directing choice. It doesn't make the characters cooler (in the positive sense, "cool" is an ambiguous word which can also be interpreted as "detached"), it makes them unrelatable. 

Also, and it may sound odd to some but if you stop to think about it, I reckon you'll agree that there are way too many combat scenes, each far longer than it needs to be. It takes away from the plot and alters your excitement threshold to a point where you're thinking: "Yes, I got it. Neo is the one, he's a demigod and he can fly like Superman (only his outfit makes him look cooler - again that word) and beat everyone's arse. Can we get on with the story, please?" 

Lastly, while watching the [Trinity computer break-in scene](https://www.youtube.com/watch?v=0PxTAn4g20U) in part II (Reloaded), I again marvelled at why Hollywood can't get these scenes right. I mean, seriously, they pour millions of dollars into these productions, how much would it cost to hire a consultant like me to make it believable? I'd have done it for the relatively humble sum of $100 (plus credits). 

Unlike most films, it seems that they DID consult someone because the use of actual utilities is depicted in the scene (`nmap` and `ssh`) and the "CRC32 exploit" reference alludes to a real CVE (see [2001-0144](https://marc.info/?l=bugtraq&m=98168366406903&w=2)) and clearly, someone also told them it's more realistic for the fictional `sshnuke` util to reset root's passwd rather than output the existing one (because passwds are hashed).
 
To me, it makes the scene more disappointing in a way. Why? Let's analyse.

Here's how the scene goes down:

Trinity sits down and gets to typing. She inputs very few characters and then we're shown:

- The truncated output of `nmap` in a readable resolution (but not the arguments used). Seconds later, we're shown the full screen from a farther angle. I couldn't read from it but I did find [this article](https://nmap.org/movies/#matrix) on nmap.org so I know the full command was `nmap -v -sS -O 10.2.2.2`
- full command (arguments included) for the fictional `sshnuke` util
- full `ssh` command to get to the mysterious 10.2.2.2
- at this point, she lands in the "RRF-CONTROL" shell (convenient!) and types: `disable grid nodes 21 - 48`

Let's break this down:

- She already has access to a super user shell (presumably, based on the fact `PS1` starts with "#" and the use of `-sS`). But you know what? I'll buy that because the scene opens with two security guards looking at a passed-out person over their desk, wondering "what the hell happened here?!". So, okay, that person had the terminal open and something happened and Trinity walked into that situation, fine.
- 10.2.2.2 is a class A (CIDR) addr. Okay but how did she know that's the machine to SSH to in order to get to the "RRF-CONTROL" interface? I mean, you want to depict the use of `nmap`? how about using `-sn` for host discovery and working things out from there?
- She invokes the `nmap` with `-v -sS -O`. Let's break these down as well:
* `-v` stands for verbose (unimportant)
* `-sS` is SYN stealth scan (half-open scan) - a technique that sends SYN packets without completing the full TCP handshake. That's the default Nmap scan type when running with super user privileges so she could have saved some typing but still, okay:)
- But now, we get to `-O`: Enable OS detection. This one is super important because it makes the scanning significantly slower. 
On my own laptop `nmap -v -sS -O localhost` takes roughly 12 seconds, whereas without `-O`, it takes a little over 0.900 seconds (try it, you'll see). 

And that brings us to the real question: why does she even need `nmap` here? CVE 2001-0144 affected many different OSes and SSH servers, not a particular one. Even if she wants to know the version of the SSH server, `telnet 10.2.2.2 22` is much faster. TCP 22 is the default port, which frankly, is the only insight `nmap` gives her (notice the "No exact OS match" in the output). `nmap` in this situation, would make sense if trying TCP 22 FAILED and she needed to discover what alt port the service listens on. Moving on...
- This `sshnuke` util, bloody F is it doing on that machine? At least show her mount a USB stick to run this thing?!
- This "RRF-CONTROL" shell:
 * How does she know its sub-commands?
 * 3 separate args to denote the node range (21,-,48), seriously?!

Hollywood producers: if you happen to encounter this post, I'm "open for work". I can help you make these scenes realistic while still keeping them tight (timeline-wise) and fast-paced:)

