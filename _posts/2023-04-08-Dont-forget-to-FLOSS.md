---
layout: post
title: Don’t forget to FLOSS!
date: 2023-04-08 16:32:47 UTC
background: //cdn-images-1.medium.com/max/1024/0*80vpeYADwFXDL8_m
---
A bar circa 1950. Four women enter. Thanks to special magic employed by certain Hollywood professionals, the leading girl grabs everyone’s attention. The camera cuts to a group of nerdy mathematicians. As so often happens, plotting and scheming ensues. The goal? Get that girl’s attention.

Semi intelligent dialogue takes place, with references to Adam Smith, among other things. Then one of them&nbsp;says:

**_“If we all go for the same girl, we block each other and nobody gets her. Then what will we do? We’ll go for her friends.. but they’ll give us the cold shoulder because no-one likes to be second choice. Here’s what we should do instead: nobody goes for that girl. We each approach one of the other girls. Everyone gets a date. The best result will come from everyone in the group doing what’s best for himself… and the&nbsp;group.”_**

If you’ve seen it, you know this is a scene from A Beautiful Mind, dialogue shortened and paraphrased.

At this point, you may be wondering what this has to do with FLOSS. At first glance? Nothing at&nbsp;all.

Actually, it barely even has anything to do with the Nash Equilibrium. The Nash Equilibrium occurs under certain conditions (and assumptions of rationality) during a simultaneous game (where all the people have to choose at the same time) without the ability to collude like they do in that&nbsp;scene.

Nonetheless, it’s a very well done scene and it got YOUR attention, which is what every author aspires to do. Also, its bottom line (“The best result will come from everyone in the group doing what’s best for themselves… **and** the group.”) does have a lot to do with this article because I will attempt to prove this is true to FLOSS and is the reason why FLOSS [Free (Libre) Open Source Software] is the best way to go. For EVERYONE.

To be honest, I usually just use the term FOSS. The ‘L’ is really only there for clarification since, in English, the word free is used in multiple contexts; think “Free beer!” vs. “I’m a free man!”. However, FLOSS is brilliant from a marketing perspective, because it grabs the eye and lends itself to some amusing puns. I find marketing to be a fascinating topic and I love puns so… here we&nbsp;are.

A lot of great volumes have been written about FOSS and its importance. It has been studied from a technical standpoint, a sociological one, an economical one and probably a few other perspectives. In this article, I’ll present my arguments for why FOSS is the best route for the following entities: developers, companies and ALL end-users, from the tech savvy to the retired Bridge playing librarians.

### From a Developer’s Perspective

Like all living organisms, developers require nourishment. For most of us, that means you need to be employed by someone who can PAY you. The code you end up writing then typically (there are exceptions) belongs to that someone. However, if you’re working on a FOSS project, while the resulting code usually does not belong to you, the **credit** for the parts you wrote **does** and that makes a huge difference!

For a developer, actual code speaks much louder than words. Yes, that includes very fancy words, properly formatted and styled, with a brilliant font and captivating icons for bullets.  
Anyone can open a thesaurus and compose sentences out of intelligent looking [rarely used] words and some people even manage to do it in a grammatically correct manner. Everyone [but me, it seems] is capable of using “advanced” word processing features to produce a very “professional” looking CV. **_But not everyone can&nbsp;code_**.

Employers and recruiters are, slowly but surely, starting to get wise to that fact. Simply put: your code is your best business card. If the code you write cannot be shared with people outside your company, your only option is to _describe_ what you’ve previously done. That’s an important skill too and one that you should acquire, no matter what it is you do (it may seem trivial but you’d be amazed by how many people fail to do just that).  
However, nothing is better than being able to _show_ people your code. To use a simple analogy: listening to an artist describing their work can be interesting but actually seeing the work tends to leave a more lasting impression.

Working on FOSS projects has some other interesting advantages.  
For one thing, it forces you to improve your written communication skills. Being able to convey your ideas clearly is a skill well worth acquiring and I can think of no better way to improve that than having to exchange words with strangers from all over the world over email, bug reports and forum threads.  
Look closely at the bullets in the job descriptions you read, I guarantee you’ll be able to spot “good written communication skills” quite&nbsp;often.

Another important advantage is that FOSS is a great equaliser. Machines don’t care about who is giving the instructions, they just follow them blindly. If you do a good job conveying your desire in the form of code, they’ll do the right thing with it [well, most of the time]. This is why I love machines. When working on FOSS projects, your gender, ethnicity, where you went to school, etc, all become immaterial [because they’re often unknown] and the only thing that matters is **_can you write good code and explain it to&nbsp;others?_**

Lastly, and this is a point whole articles, social studies and even books can be based on [and probably were]: **_being involved with FOSS can do wonders to your&nbsp;ego_**.

### What’s in It for Companies?

I live in a capitalistic world. Since you’re reading this, I imagine you do&nbsp;too.

Unlike with FOSS, I’m not sure capitalism is the best way to go but I also don’t have a better solution so.. I go with&nbsp;it.

In a capitalistic world, commercial entities are formed with one goal in mind: TO MAKE&nbsp;MONEY.

Some people and therefore companies have difficulties reconciling FOSS philosophies with this goal and the ambiguity of the word ‘free’ can’t be helping&nbsp;much..

In actuality, the two are hardly mutually exclusive, quite the contrary, in fact. If you don’t believe me, just ask RedHat, they seem to be doing quite well. And they are far from being the only example,&nbsp;either.

Here is what you need to understand about FOSS and money: **it’s all about your value proposition**.

If your sales pitch goes a little something like&nbsp;this:

**_“Our software does BLAH better than anything else in the market. We do BLAH faster and more efficiently than all our competitors because we employ very smart people who devise very secret algorithms and simply put: we’re the bloody best, just take our word for it.. and if you pay us a lot of money, we’ll grant you the privilege of using our product and reporting the bugs you find. Let me show you a&nbsp;demo…”_**

Then, you, my friend, have a serious problem on your hands. Needless to say, with this value proposition, you cannot make your code openly available but worse still, your revenue relies solely on the premise that no-one will ever come up with a product that does BLAH better. But, as history teaches us, someone&nbsp;WILL.

Now, let’s consider an alternative value proposition:  
**_“Our software has a unique approach towards BLAH that we think is quite clever. Here are some posts and technical documents explaining what we do and why we think it’s the best&nbsp;way._**

**_Our methods, algorithms and actual code are open and free. Our code is being used and developed by a lot of people, inside and outside of our company. We’d love for you to look, test and comment on it. We expose APIs that allow you to utilise our product in many different ways. Here are some examples of what others are doing with it. Oh, and we also offer official support, professional services and custom development work. These last few cost money, let’s talk about them after you’ve had some time to asses our product.”_**

As a prospective customer, which approach sounds better to **YOU**? Yeah, thought&nbsp;so.

Okay, so other than sounding better to prospective customers (which is a lot, really), what other advantages does FOSS offer to commercial entities? Quite a few, as it&nbsp;happens.

Here are two clear advantages (curiously enough, they revolve around code quality and free&nbsp;labour):

- When developers know everyone can see their code, they write better code. It’s a simple human trait: we don’t want to be&nbsp;shamed.
- When your software is free and open, you reach a far wider audience. Therefore, your software will be tested in more ways than any of your product and QA people ever thought imaginable. If you do a good job encouraging your users, that will mean free QA, security auditing, bug fixing, technical writing and even spell checking services.

### Business Challenges

Just because it’s the best approach, does not mean it’s always&nbsp;easy.

For starters, as I explained, you need to come up with a business model that enables you to produce FOSS while generating revenue.

But there’s another, less obvious point you must give consideration to:

**_Making your code freely available is not the only thing you need to do to properly benefit from&nbsp;FOSS._**

It’s clearly a good and mandatory start, but there’s more to the story. To make your project truly popular and widely used, you need to encourage interaction.

By this I&nbsp;mean:

- You need to provide documentation. It doesn’t have to be perfect (and I promise you it will improve over time) but it has to be&nbsp;there
- You need to provide a simple way for people to ask questions and report issues (bug tracking systems, forums, etc. But don’t worry, there are a lot of great tools for that out there, both free as in free beer and free as in&nbsp;open)
- You need to provide a simple way for others to become involved and help the project and remember: help can come in the form of bug reports, documentation and assisting other users. It’s not **_just_** about code contributions, although these are very important

Generally speaking, you need to make the decision-makers (developers, product managers, sometimes even the purely business-oriented folks), accessible and available for dialogue.

Don’t get me wrong, this is a huge time investment for a company and in some individual cases, it can be frustrating (some users have crazy use cases in mind that no one else is interested in and you’ll have to explain that to them, **_politely_** , some will never read your documentation and bombard you with invalid bug reports, a few will behave as if you owe them something, even though they have not paid you a single dime nor do they plan to) but even with all these real-life examples in mind, it is still very much worth&nbsp;it.

### End Users

People sometimes think FOSS is only relevant or beneficial if you are a developer or at least, very tech savvy. That is not so. Non-technical users also benefit greatly from using FOSS (without even knowing&nbsp;it).

Here are, perhaps the most obvious advantages:

#### FOSS protects the user from vendor lock-in (also known as proprietary lock-in or customer lock-in).

Vendor lock-in makes a customer dependent on a vendor for products and services and thus unable to use a different vendor without substantial switching costs.

#### Support

While user communities do sometimes form around proprietary products, FOSS products/projects are far more likely to have active, eager to help community members. People tend to be far more passionate about FOSS, where they can make a real difference than they do about proprietary software where they have far less impact on the product. And so, if the product you’re using is FOSS, you can get help and guidance from other users, as well as the company that releases it. If the product is very popular, this can make a very substantial difference in both quality and response&nbsp;time.

**Better code**  
To reiterate my point from the `What’s in it for Companies?` section: the more people exposed to the code, the better it will become. This benefits both the company and the end-user.

### Excuses, FOSS Leeches, and How to Avoid Being&nbsp;One

Some companies are afraid that if they open their product, others will copy it. In reality, if your product is popular, others will copy it regardless of whether or not it is open. Granted, FOSS makes it much easier to see how you’ve implemented things but if you’re successful, the fact your code is proprietary will not prevent others from imitating you in order to capitalise on your investment. On the other hand, being open may induce users to contribute to your project, rather than start a competing one.

In some cases, your business model may be such that opening the software will damage your profitability, but generally speaking, if you can build a model where being FOSS does not only hinder profitability but actually increases it (by the various means discussed in this article), then you&nbsp;should:)

Even if your product is not FOSS, you may still be using FOSS components. In fact, it is very likely indeed. Depending on the license of these components, it may be perfectly legal to do so but of course, you must check and confirm that that is the&nbsp;case.

Legal considerations aside, one good turn deserves another. If you’re using FOSS, it only stands to reason that you will contribute something back. When I use the term “FOSS leeches”, I am referring to individuals/companies who make a living off of FOSS but fail to do&nbsp;so.

Here are just a few ways by which you can contribute to FOSS without actually being open yourself:

- Submit bug reports/patches to the maintainers of the FOSS components you use (don’t just patch the original without letting anyone&nbsp;know)
- Become an active member of the community and answer questions from other&nbsp;users
- Write documentation and blog posts describing how you leverage the FOSS projects you&nbsp;use

In addition to the above, remember that even if, for whatever reason, your actual product cannot be FOSS, there are usually many bits of software around it that you can release as FOSS without hurting your business at all. For example, if you’ve written some helpful deployment procedure/code that is generic and can be used in other projects, you can certainly make it available so that others may benefit. Same goes for testing procedures.

In conclusion: since FOSS enables you to do well, the least you can do is also do some&nbsp;good.

 ![](https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=25f3faa3856e)