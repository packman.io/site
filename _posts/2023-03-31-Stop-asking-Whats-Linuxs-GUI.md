---
layout: post
title: Stop asking “What’s Linux’s GUI?”
date: 2023-03-31 20:13:06 UTC
background: //cdn-images-1.medium.com/max/1024/0*8qfqoYQJFdPhxGM3
---
I am often asked a variation of this question and other similar ones that simply do not make any sense when you understand how things actually work. Up until now, I’ve tried my best to answer them without looking overly put off, when asked by people I appreciate, and with complete annoyance, when asked by people I do&nbsp;not.

The purpose of this post is not to put down people who do not [yet] have an understanding of what Linux and the layers running on top of it are, nor is it to sell Linux to anyone. It is a humble attempt to educate those who are interested in gaining a basic understanding on the topic so they can make their choice out of knowledge, rather than ignorance.

A secondary purpose is to optimise my time, since, from now on, instead of repeating myself, I could refer people to it and let them gain their understanding, if they are truly interested in understanding, rather than just talking my head&nbsp;off:)

It was [Richard Feynman](https://en.wikipedia.org/wiki/Richard_Feynman) who said: _“If you can’t explain it simply, you don’t really understand it.”_ I am hardly the genius he was, but I’ll give it my best shot… In order to explain why the question “What’s Linux’s GUI” is not a valid question, we must first explain what Linux&nbsp;is.

From the Linux kernel’s&nbsp;[README](https://github.com/torvalds/linux/blob/master/README):

_“Linux is a clone of the operating system Unix, written from scratch by Linus Torvalds with assistance from a loosely-knit team of hackers across the Net. It aims towards POSIX and Single UNIX Specification compliance.”_

I will not explain what [UNIX](https://en.wikipedia.org/wiki/Unix) is in this article but it is important to note that Linux is a UNIX based operating system and I do encourage everyone who is not familiar with it, to read about its&nbsp;history.

So, Linux is an operating system, or more accurately, a computer operating system. Now let us see what defines an operating system; from Wikipedia [https://en.wikipedia.org/wiki/Operating\_system](https://en.wikipedia.org/wiki/Operating_system):

_“An operating system (OS) is system software that manages computer hardware and software resources and provides common services for computer programs.”_

That is not a bad definition but I do feel it is a bit vague. I therefore prefer this one from [http://whatis.techtarget.com/definition/operating-system-OS](http://whatis.techtarget.com/definition/operating-system-OS):

\*“An operating system is the program that, after being initially loaded into the computer by a boot program, manages all the other programs in a computer. The other programs are called applications or application programs.

The application programs make use of the operating system by making requests for services through a defined Application Program Interface (API). In addition, users can interact directly with the operating system through a user interface such as a command line or a graphical user interface (GUI).”\*

So, following this definition, it is rather easy to deduce that… **TADA — an operating system does not require GUI at&nbsp;all!**

Historically, this terribly confusing and bad coupling of the operating system [OS] and the GUI, started with MS Windows. UNIX existed quite happily since the mid-1960s without any GUI at all, other operating systems, including quite a few listed here [https://en.wikipedia.org/wiki/Timeline\_of\_operating\_systems](https://en.wikipedia.org/wiki/Timeline_of_operating_systems), did&nbsp;too.

And some of us, yours truly included, also worked with DOS, which had no GUI whatsoever.

**I have often heard Linux described, in complete ignorance, as a “DOS like operating system”!!**

And why? Because people are far too visual and far too happy to think they KNOW something because it “looks like” something else they think they know. And so, people see me work on my Linux in a pseudo terminal, which sometimes, has a black background and a white font [though it can easily be changed and often is] and think about where they last saw something “similar” and for many, that “something similar” is a remote childhood memory of&nbsp;DOS.

In actuality, DOS and Linux [or any other UNIX like system] have very little in common apart from the fact both provide a Command Line Interface [CLI] and both are operating systems. There are so many advantages to UNIX over DOS that the comparison is completely absurd but again, that is not the main topic of this post so I will not further develop this&nbsp;line.

OK, I feel we’ve established that an operating system does not, in fact need a GUI at all in order to operate. But, if you’ve ever seen a Linux DESKTOP before, you must have noticed that it does, usually, run some sort of GUI. Indeed, my own laptop runs Linux and ON TOP OF IT,&nbsp;GUI.

And now, we come to what is perhaps UNIX’s biggest advantage: it is modular. The very basic idea behind UNIX is that you can accomplish any task by piping together different tools. To explain the notion of modularity, let us use Lego pieces as an analogy: you buy a Lego set and you can build different things combining these Lego pieces together.

In UNIX in general and in Linux specifically, you can plug a lot of different things on top of your kernel Lego piece and, you have choices. You can talk to your Linux in command line ONLY, using a shell, which is worth an article by itself. You can also talk to it by writing code in C that calls the kernel APIs, you can use TUIs [Text User Interfaces] and, yes, if so inclined, you can use a&nbsp;GUI.

On my Linux servers, I have no GUI at all, nor do I need GUI. This is not just some crazy quirk only I have, MOST people will NOT use GUI on their Linux servers. There are many reasons for that and that too, is worth yet another post. I just hope I’ll find the&nbsp;time..

But back to our main topic: since there is no coupling between the kernel and any particular GUI, many alternatives exist. If you have decided to use GUI on top of your Linux kernel, the first thing you’ll need is the X Window server or Wayland, which is intended as a simpler replacement for X. If I were Feynman, it would probably have been easier for me to explain what that is, but I’ll try my&nbsp;best:)

An X server provides the basic framework for a GUI environment: drawing and moving windows on the display device and interacting with a mouse and keyboard. X does NOT mandate the user interface — this is handled by programs that interact with it, namely: Window Managers. As such, the visual styling of X-based environments varies greatly; different programs may present radically different interfaces. Again,&nbsp; **MODULAR**.

So, we have our kernel which is the core of our system and now we have X, which knows how to draw graphics by utilising our display device driver, essentially, displaying graphics on our screen. Already, we have two alternative choices for this function: [X server](https://en.wikipedia.org/wiki/X_Window_System) or&nbsp;[Wayland](https://wayland.freedesktop.org/).

Now that we have that, we can choose a window manager and a desktop environment!

A window manager is system that controls the placement and appearance of windows within a windowing system in a graphical user interface. On UNIX systems, it uses X and instructs it how to style the graphical elements it wants to display. But in order to provide a complete GUI experience, just drawing windows on the screen is not enough. Users who want to use GUI to operate their computer, want a set of tools or utilities or applications [choose your favorite word] that lets them control their environment. For example, they want to be able to choose their wallpaper and default font and they want to use GUI to connect to the internet and setup their hostname and see what programs they have installed and install new applications and browse their files so on and so on… And this, is where a Desktop Environment comes&nbsp;in.

**What is a desktop environment?**

A desktop environment (DE) is an implementation of the desktop metaphor made of a bundle of programs running on top of a computer operating system, which share a common graphical user interface (GUI). Desktop environments will typically include their own Window Manager but modular being the name of the game, the default window manager can usually be&nbsp;changed.

So, you may decide that you like one desktop ENV for its utilities but don’t really like its window manager and set it up so that it uses an alternative one. You may also decide that you love MOST of the tools one desktop ENV has to offer but don’t like its terminal emulator and choose to install one created by a different project that provides a desktop&nbsp;ENV.

For example, I use Debian [GNU/Linux](https://debian.org/).

Debian is a Linux distribution.

It means that it takes the Linux kernel and huge amount of free open software from a variety of projects and packages them all nicely together so that I won’t have to compile and configure it all by myself [even though sometimes it’s fun to&nbsp;do:)].

There are many many other Linux distributions, including: RedHat, CentOS [which is based on RedHat], Gentoo, Slackware, Ubuntu [which is based on Debian] and many many more. A good place to find a rather complete list is distrowatch.com

By default, Debian suggests GNOME as its desktop ENV. GNOME is a very famous and perhaps the first full desktop environment to run on Linux. GNOME is developed by GNU, an organisation that is certainly worth its own post and I do encourage you to read about them, their mission and their legendary leader Richard Stallman.

I have endless amounts of respect for GNU and I use a lot of their software every day, including BASH, GCC and many many other projects I could not live without, but, ever since GNOME 3 was released, I personally no longer like using it as my desktop ENV. Therefore, when I install a new Debian machine, I also install LXDE, which is an alternative desktop ENV that I do like and also Konsole, which is a terminal emulator that is part of an entirely different, popular and well respected desktop ENV called KDE. I’ve only named three desktop ENVs in this article, there are probably more than a&nbsp;100!

And this, in about a 100 lines, is why “What’s Linux’s GUI?” — is the wrong question!

 ![](https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=f64a473cc486)