---
layout: post
title: shellcheck and ALE demo
date: 2023-07-09 22:57:36 UTC
background: /img/asciinema/shellcheck_vid.jpg
---

Use [shellcheck](https://www.shellcheck.net) and [ALE](https://github.com/dense-analysis/ale) to lint your shell scripts on the fly in VIM.

Thanks to [ASCIInema](github.com/asciinema), you can copy everything displayed in the shell right off the player. Simply pause, mark and copy as you normally would. If you prefer, you can also download the file and play it locally in your own shell with `asciinema`.

Enjoy:)

{% asciicast 595695 %}

