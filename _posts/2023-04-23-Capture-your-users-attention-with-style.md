---
layout: post
title: Capture your users attention with style
date: 2023-04-23 20:23:44 UTC
background: //cdn-images-1.medium.com/max/1024/1*TlDFO_bhcRPJDMxEceyeyw.png
---
Do you manage UNIX machines that are logged into by multiple users? If so, this scenario will be familiar to you: **you need to communicate something to your users — a maintenance window, modifications following an update, a policy change, etc,&nbsp;etc.**

What do you do? One of&nbsp;these:

- If you’re a UNIX veteran (which, really, you **ought** be to manage important machines), you either edit /etc/motd or use wall to inform your users (or both, depending on the&nbsp;case).
- Some people aren’t even aware of these lovely utils so instead, they email, or worse (from a spamming standpoint) — they IM “groups” or “channels” (terminology varies depending on the comm tool used by the&nbsp;org).

At this point, you may be wondering “_So? What’s wrong with that?!_”.  
Well, I’ll tell&nbsp;you…

Let’s start with `/etc/motd` (short for Message Of The Day). For those who do not know, it is a text file whose contents are outputted upon user login. It’s a straightforward, elegant mechanism. Alas, it was created in simpler times, when people actually read stuff and you were allowed to respond with “RTFM!” and laugh at them when they didn’t (I miss these days! but I also miss Perl and there’s nothing one can do about either— times changed).   
Today, many distros output so many messages upon login by default (and admins do not bother editing these or the scripts that generate them) that users actually train their minds to **pointedly ignore&nbsp;them.**

As to `_wall_`, it’s a util that outputs a given string to the tty (or, more commonly the pts), of all logged-in users. Again, it suffers from the same problem: if a user is happily typing away commands or tailing some log file, he or she may not notice the message at&nbsp;all.

I deliberated whether I should even have to explain why emailing about such things is bad and decided that I should, providing that I can keep it short:  
Basically, in the case of policy changes or post upgrade updates, people tend to ignore the notification altogether, even if they did notice it; as to maintenance windows, you can make the message sound important enough by including “ATTENTION” or “MUST READ” (or words to that effect) in your subject line but the question is: when to send this? If you send it ahead of time, people forget all about it by the time it takes place. If you send it just before, people may not see it as no one reads emails anymore (except for me — I love emails). So, after being burnt a few times, you end up sending it several times which is annoying to do but, even more annoyingly — STILL not&nbsp;enough.

Okay, now that I got your attention (well, one would hope), what is my proposed solution?

In 3 words? USE THIS&nbsp;[SCRIPT](https://gitlab.com/-/snippets/2531799)

It’s pretty self explanatory once you run it but I’ll elaborate a bit anyway. (I’m told the most read Medium posts are estimated at around 7 minutes read time so I reckon I’ve got a few paragraphs to&nbsp;spare).

This script uses the [toilet](https://github.com/cacalabs/toilet) and [lolcat](https://github.com/busyloop/lolcat) utils to generate messages that will grab your user’s attentionand the who util to find the tty and pts devices of logged-in users so it can send them said messages.

To illustrate, here’s what it outputted to the terminal when invoked&nbsp;with:

    $ ./gmessage.sh “This system will go down for maintenance in 7 minutes” Jesse

![](https://cdn-images-1.medium.com/max/1024/1*9W0pxZIYQqRd2VvtefDAQA.png)

That would be kind of hard to ignore,&nbsp;right?

So, this script serves as a pretty good [wall](https://github.com/util-linux/util-linux/blob/master/term-utils/wall.c) replacement (wall will strip all escape/control sequences other than \007, by the&nbsp;way).

As to an alternative for /etc/motd, how about some custom code in `/etc/bash.bashrc` (or the equivalent in your interactive shell, BASH isn’t for everyone)?

For example, I’ve added this to `/etc/bash.bashrc` (`zsh` and friends are nice but I like my&nbsp;BASH):

    
    if [[-n $SSH_CONNECTION]] ; then
    	toilet "Welcome to $HOSTNAME" -f future.tlf -t | /usr/games/lolcat
    	echo -en "Here are some things we need to let you know about.\nBlah blah blah and also, tripe, tripe, rubbish\nThank you\n-- root" | /usr/games/lolcat
    fi

And, here’s the&nbsp;result:

![](https://cdn-images-1.medium.com/max/1024/1*6Uyf9cnFd2fCVgUzE_vPSA.png)

Nice, eh? Let’s see them say they didn’t see that&nbsp;one:)

Both toilet and lolcat support multiple flags so it’s worth glancing at their respective man pages. For toilet, I am using the future font file by passing “-f future.tlf” but if you don’t like it, there are plenty others to choose&nbsp;from.

I installed both from Debian’s official repos and as a result, the fonts reside under `/usr/share/figlet`, the location on your box may&nbsp;vary.

Lastly, another awesome use of these utils is generating captivating headers from the comfort of your shell:) I used it to generate the hero image for this post (far faster and easier than any presentation/animation tool I’ve ever worked&nbsp;with).

Happy hacking to&nbsp;all!

 ![](https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=42d8562cd7c2)
