---
layout: post
title: Docker is not a packaging tool — intro
date: 2023-04-04 20:59:22 UTC
background: //cdn-images-1.medium.com/max/1024/0*ZO0_8Ld6bnGvSRPr
---
This series of posts will take you all the way from chrooted ENVs to Docker containers and attempt to explain why, while Docker is a great tool, proper packaging of software remains as relevant as&nbsp;ever.

If you’re reading this, you probably already heard of Docker and likely also used it; if not for your own projects then to deploy others’. And so, you may think there’s nothing I could tell you about it that will surprise you.. Let’s find out, shall we? Give it a go, I will try to make it amusing,&nbsp;too:)

If you go to [https://en.wikipedia.org/wiki/Docker\_(software)](https://en.wikipedia.org/wiki/Docker_(software)), the first paragraph you’ll encounter is: _“_ **_Docker_** _is a set of_ [_platform as a service_](https://en.wikipedia.org/wiki/Platform_as_a_service) _(PaaS) products that use_ [_OS-level virtualization_](https://en.wikipedia.org/wiki/OS-level_virtualization) _to deliver software in packages called_ [_containers_](https://en.wikipedia.org/wiki/Container_(virtualization))_.”_

Packages called containers?! Eh, let’s take a step back from tech terms and discuss English; **Container** , as defined by Oxford: _“an object for holding or_ [_transporting_](https://www.google.com/search?client=firefox-b-e&sxsrf=APwXEddj7Nl83J1RsbxlOTy327nrCceV1Q:1680385537848&q=transporting&si=AMnBZoFOMBUphduq9VwZxsuReC7YZB-GgKujLv6p8BFX2GIRJnMlxU3HgdYDP7WYg6boTkRRuaU0gl_y0LXpbTy5gXz5GZgtH4cIZAr9sckuI3pUJWE9VS0%3D&expnd=1) _something.”_

Yeah, matches my understanding of the word, wouldn’t you&nbsp;agree?

Okay, now the same for **Package:** _“an object or group of objects wrapped in paper or packed in a&nbsp;box.”_

Again, fair enough,&nbsp;right?

I hate metaphors but I do like analogies and this one is pretty good so, try this out for size: there’s a container, filled with packages, it sits on the docks in the harbour. At some point, the crew will go in and unpack these packages and, after some processing, they’ll eventually arrive at their different destinations.

Now, is a container the same thing as a package? That’s right, it isn’t. Definitely not in the physical world but as I’ll attempt to explain — not in software, either.

Before we do that, though, let’s give Wikipedia another chance (it deserves it) and see if we can find some interesting paragraphs about Docker that I don’t&nbsp;dispute…

_“Containers are isolated from one another and bundle their own software,_ [_libraries_](https://en.wikipedia.org/wiki/Library_(computing)) _and configuration files; they can communicate with each other through well-defined channels. Because all of the containers share the services of a single_ [_operating system kernel_](https://en.wikipedia.org/wiki/Kernel_(operating_system))_, they use fewer resources than_ [_virtual machines_](https://en.wikipedia.org/wiki/Virtual_machine)_.”_

See? told you they deserve a chance. This is a good, succinct description of some of the benefits of containers. They indeed have a smaller footprint than VMs and that’s a big plus but I’d like to focus on the isolation bit for a moment. Isolation provides two main advantages:

- Security
- [Relative] Decoupling

Before we go on to explain how Docker helps us with the above, I feel it would be nice to honour what I personally consider its first predecessor — The&nbsp;Chroot.

Docker launched in 2011 and started gaining traction circa 2013 but, long before that, a simpler, widely used UNIX tool gave us the benefits of isolated (often referred to as jailed)&nbsp;ENVs.

**So, what’s this `chroot` thing,&nbsp;then?**

Let’s give Wikipedia some well deserved rest and go to another beloved resource — the man&nbsp;pages.

    $ man -k chroot
    chroot (2) - change root directory
    chroot (8) - run command or interactive shell with special root director

In case you’re not very familiar with man pages, they consist of different sections and since I think this is something that’s useful to know, I’ll list the different sections&nbsp;below:

    MANUAL SECTIONS
    The standard sections of the manual include:
    
    1 User Commands
    2 System Calls
    3 C Library Functions
    4 Devices and Special Files
    5 File Formats and Conventions
    6 Games et. al.
    7 Miscellanea
    8 System Administration tools and Daemo

Okay, so from the above, we can already gather that, like many other UNIX concepts, chrootis both a syscall (section 2) and a command/tool (section&nbsp;8).

Let’s see what man 2 chroot has to tell&nbsp;us:

_chroot() changes the root directory of the calling process to that specified in path. This directory will be used for pathnames beginning with /.  
 The root directory is inherited by all children of the calling&nbsp;process._

_Only a privileged process (Linux: one with the CAP\_SYS\_CHROOT capability in its user namespace) may call chroot()._

Interesting, right? Now man 8&nbsp;chroot:

_chroot — run command or interactive shell with special root directory_

Right, so, unsurprisingly, the command chroot calls the syscall chroot...

Let’s go back to Wikipedia for a brief history lesson — trust me, these titbits make for great conversation started, be it in job interviews, work lunches and dates (well, okay, maybe less so on dates but it really does depend on whom you date, doesn’t&nbsp;it?):

_The chroot system call was introduced during development of_ [_Version 7 Unix_](https://en.wikipedia.org/wiki/Version_7_Unix) _in 1979. One source suggests that_ [_Bill Joy_](https://en.wikipedia.org/wiki/Bill_Joy) _added it on 18 March 1982–17 months before_ [_4.2BSD_](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution) _was released — in order to test its installation and build&nbsp;system._

So.. this was 1982. Good vintage, one hopes, I was born nearly three months later, Docker was born 29 years later but I’d like to take us near the present and regale you with some stories of how I had used chrooted ENVs back in 2006–2010.

At the time (I was much younger and looked like a young brad Pitt — well, no, I absolutely didn’t but I bet you laughed at that one) I worked for a company that the PHP veterans amongst you will undoubtedly have heard of called Zend and held the best title I ever had by far — Build&nbsp;Master.

Remember, this is 2006. SaaS was already a concept but it was not as widespread as it is today and far more companies delivered software that was meant to be deployed on the customer’s machines (in 20 years’ time, some youngster will find this a novel idea, I’m&nbsp;sure).

Zend was one such company and the software it delivered (amongst other product lines) was a PHP application server (think JBOSS but for PHP, basically). First it was called Zend Platform, it then had a multitude of internal codenames that drove me bunkers and ultimately, it was rebranded as Zend&nbsp;Server.

So, what did the build master do? Well, since the product was to be deployed on customer machines and seeing how Zend initially wanted to support anything that has uname deployed on it and the product consisted of many different components written in C/C++, which in turn, depended on third party FOSS components (also written in C/C++), as well as a PHP admin UI [pause for breath] someone had to build and package all these things. I, along with two excellent mates of mine, was one of these&nbsp;people.

In the [next instalment](https://packman.io/2023/04/18/Docker-is-not-a-packaging-toolpart-1.html) of this series, I’ll tell you about the challenge of delivering software to multiple UNIX and Linux distributions (same same but different) and how chroot was leveraged towards this objective.

We’ll then get back to Docker and why it is the next evolutionary step and, finally, explain why Docker is only part of the dream deployment process and even note some cases where it’s not that&nbsp;helpful.

Stay tuned and, if you like this sort of content (but only if you do — don’t feel obligated), please give the clapper a go and follow&nbsp;me.

Happy building.

 ![](https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=e494d9570e01)
