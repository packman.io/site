# packman.io

This repo contains all files needed to generate [https://packman.io](https://packman.io) using [Jekyll](https://jekyllrb.com)

## Installation & Setup

- [Install Jekyll](https://jekyllrb.com)
- Clone this repo or download the source code archive
- Edit the configuration in `_config.yml` file
- Build your site: 
```sh
# to generate your site:
$ bundle exec jekyll build

# to serve locally over port 4000 invoke:
$ bundle exec jekyll serve
```
- Once generated (files will be placed under `$ROOT_DIR/_site` by default), you can configure any web server to serve your pages. See the Jekyll docs for more info

### Contact form
This site uses [Formspree](https://formspree.io) to send emails with the posted messages.
Create an account and set the `formspree_id` in `_config.yml` and you're all sorted. 

### Post Comments are handled using `Isso`
See documentation at https://github.com/posativ/isso.

## Influences/credits

When creating this site, I adopted code from:

- [Start Bootstrap - Clean Blog Jekyll](https://startbootstrap.com/themes/clean-blog-jekyll/)
- [Templatemo Lavish template](https://templatemo.com/live/templatemo_458_lavish)
- Portfolio cards inspired by [Mohamed Yousef's design](https://codepen.io/a7rarpress/pen/GRYZGXo)


## Bugs and Features

- Have a bug or an issue with this template? [Open a new issue](https://gitlab.com/packman.io/site/-/issues).
- Added a nice functionallity you reckon others may enjoy? [Submit a merge request](https://gitlab.com/packman.io/site/-/merge_requests)

## Copyright and License

Copyright © Jesse Portnoy 2023. Code released under the [AGPLv3](https://gitlab.com/packman.io/site/-/blob/main/LICENSE) license.
